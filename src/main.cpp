#define CURSOR_DATABASE_FILE "/tmp/cursor.sql"
#define CURSOR_CAPTURE_RATE 250

#include <sqlite3.h>
#include <chrono>
#include <csignal>
#include <iostream>
#include <thread>

#include "io/data_wrapper.hpp"

using namespace std;

DataWrapper data_wrapper = DataWrapper(CURSOR_DATABASE_FILE);

void handle_signal(int signal) {
  if (signal == SIGINT) {
    while (true) {
      if (!data_wrapper.get_busy_status()) break;
    }

    exit(EXIT_SUCCESS);
  }
}

void handle_sqlite3_error(void* pArg, int iErrCode, const char* zMsg) {
  fprintf(stderr, "(%d) %s\n", iErrCode, zMsg);
}

int handle_commit(void* p) {
  data_wrapper.set_busy_status(false);
  return 0;
}

int main() {
  signal(SIGINT, handle_signal);
  sqlite3_config(SQLITE_CONFIG_LOG, handle_sqlite3_error, 0);

  if (data_wrapper.init_cursor()) {
    sqlite3_commit_hook(data_wrapper.get_database(), handle_commit, 0);

    while (true) {
      this_thread::sleep_for(chrono::milliseconds(CURSOR_CAPTURE_RATE));
      if (!data_wrapper.capture_cursor()) break;
    }
  }

  return 0;
}
