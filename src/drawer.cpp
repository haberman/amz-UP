#define CURSOR_DATABASE_FILE "../assets/cursor.sql"

#include <chrono>   // chrono::system_clock
#include <ctime>    // localtime
#include <iomanip>  // put_time
#include <iostream>
#include <string>  // string

#include <sqlite3.h>
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>

#include "functions.hpp"
#include "graphics/color_gradient.hpp"
#include "io/data_wrapper.hpp"

// #include "graphics/archipelago.hpp"
#include "cgal/kd_tree.hpp"
#include "cgal/triangulator.hpp"
#include "cgal/voronoi.hpp"

using namespace std;
using namespace sf;

const string ASSETS_FOLDER = "../assets/";
const unsigned WIDTH = 1600;
const unsigned HEIGHT = 900;

void handle_sqlite3_error(void* pArg, int iErrCode, const char* zMsg) {
  cout << "ERROR: ";
  fprintf(stderr, "(%d) %s\n", iErrCode, zMsg);
}

void string_replace(string& str,
                    const string& oldStr,
                    const string& newStr) {
  string::size_type pos = 0u;
  while ((pos = str.find(oldStr, pos)) != string::npos) {
    str.replace(pos, oldStr.length(), newStr);
    pos += newStr.length();
  }
}

int main() {
  bool drawn_once = false;

  sqlite3_config(SQLITE_CONFIG_LOG, handle_sqlite3_error, 0);

  DataWrapper data_wrapper = DataWrapper(CURSOR_DATABASE_FILE);

  vector<Vector2f> points;
  data_wrapper.get_cursor_points(&points);

  cout << "cursor.sql has: " << points.size() << " points" << endl;

  /** Creates the rendering target that is common to both `screen` and `file` output. */
  sf::RenderTexture render_texture;

  if (!render_texture.create(WIDTH, HEIGHT)) {
    cout << "[ERROR] can't create texture." << endl;
    return EXIT_FAILURE;
  }

  shared_ptr<IntRect> texture_space = make_shared<IntRect>(0, HEIGHT, WIDTH, -HEIGHT);
  shared_ptr<ColorGradient> theme_gradient = make_shared<ColorGradient>(GradientStyle::WeddingDayBlues);

  Triangulator* triangulator = new Triangulator(theme_gradient);
  Voronoi* voronoi = new Voronoi(theme_gradient);
  KdTree<float>* kd_tree = new KdTree<float>(theme_gradient);

  for (auto point : points) {
    const float map_x = Functions::map_number(point.x, 0.f, 1.f, -1.f, 1.f);
    const float map_y = Functions::map_number(point.y, 0.f, 1.f, -1.f, 1.f);

    const sf::Vector2f p{map_x, map_y};
    voronoi->insert(p);
    kd_tree->insert(p);
    triangulator->insert(p);
  }

  //   Archipelago archipelago(screen);
  //   archipelago.add_points(points);

  Sprite sprite(render_texture.getTexture());
  sprite.setTextureRect(IntRect(0, HEIGHT, WIDTH, -HEIGHT));

  const VideoMode video_mode = VideoMode::getDesktopMode();
  const Vector2i window_position = Vector2i((video_mode.width - WIDTH) * .5f,
                                            (video_mode.height - HEIGHT) * .5f);

  ContextSettings context_settings;
  context_settings.depthBits = 24;
  context_settings.antialiasingLevel = 6;

  RenderWindow window(VideoMode(WIDTH, HEIGHT), "amzUp Drawer", Style::Default, context_settings);
  window.setSize(Vector2u(WIDTH, HEIGHT));
  window.setPosition(window_position);
  window.setVerticalSyncEnabled(true);
  window.setActive(true);

  glViewport(0, 0, (GLsizei)WIDTH, (GLsizei)HEIGHT);

  if (!render_texture.setActive(true)) {
    cout << "[ERROR] can't activate texture." << endl;
    return EXIT_FAILURE;
  }

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  // run the program as long as the window is open
  while (window.isOpen()) {
    // check all the window's events that were triggered since the last iteration of the loop
    Event event;
    while (window.pollEvent(event)) {
      if (event.type == Event::Closed) {
        window.close();
      }

      else if (event.type == Event::Resized) {
        // adjust the viewport when the window is resized
        glViewport(0, 0, event.size.width, event.size.height);
      }

      if (event.type == Event::KeyPressed) {
        if (event.key.code == Keyboard::Escape) window.close();
        if (event.key.code == Keyboard::C) Functions::save_screenshot(window, ASSETS_FOLDER, "amz-UP");
      }
    }
    // clear the buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (!drawn_once) {
      glClearColor(0.f, 0.f, 0.f, 1.f);
      kd_tree->draw(&render_texture);
      voronoi->draw();
      // triangulator->draw();
      drawn_once = true;
      //   archipelago.draw_tree();
      // archipelago.draw_delaunay();

      //             archipelago.print_tree();
    }
    Sprite sprite(render_texture.getTexture());
    sprite.setTextureRect(IntRect(0, HEIGHT, WIDTH, -HEIGHT));

    window.draw(sprite);
    window.display();

    // TODO: @FIXME
    //         archipelago.draw_delaunay();

    //         archipelago.add_points ( points );
    //         archipelago.draw_tree ();
    //         archipelago.clear();
  }

  return 0;
}
