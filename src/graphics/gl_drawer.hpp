#pragma once

#include "../geometries/delaunay/delaunay_edge.hpp"
#include "../geometries/delaunay/delaunay_triangle.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>

using namespace sf;

/** @addtogroup draw
 *  @{
 */

/** How gradients are meant to be interpolated over geometry. */
enum GradientType {
  PER_LINE,   /**< Over the entire line. */
  PER_WEIGHT, /**< Over end point as their normalized min/max weight express the connections amount. */
  PER_AMOUNT  /**< Over the amount of junxions inside the partition of a quad tree. */
};

/** Small `struct` holding the main attributes of any drawing calls. */
struct GLStyle {
  Color fill_color = Color::White;    /**< Fill color (default: black). */
  Color outline_color = Color::Black; /**< Outline color (dafault: black). */
  GLfloat outline_width = 1.f;        /**< Outline width (default: 1). */
};

/**
 * This all-static class offers shortcuts to draw more or less complex things.
 * All methods expects `GL` normalized (-1/1) coordinates in the form of a `sf::Vector2f`.
 */
class GLDrawer {
 public:
  /**
     * Draws a single line of text.
     */
  static void draw_text(RenderTexture* render_texture,
                        const Vector2f& position,
                        const string& content,
                        const Font& font,
                        const unsigned short padding = 6,
                        const unsigned short size = 12,
                        const Text::Style style = Text::Style::Regular,
                        const Color& text_color = Color::Black,
                        const Color& bg_color = Color::White);

  /**
     * Draws a circle.
     * 
     * @param position The position the circle
     * @param radius   The 2D standard radius of the circle (might be affected by junxion weight during drawing)
     * @param steps    Amount of iterations (sides) used to generate the circle (default: `32`)
     * @param gl_mode  `GLenum` that tells how vertices should be linked together (default: `GL_TRIANGLE_FAN`)
     */
  static void draw_circle(const Vector2f& center,
                          const Vector2f& radius,
                          const unsigned int steps = 32,
                          const GLenum gl_mode = GL_TRIANGLE_FAN);

  /**
     * Draws a rectangle.
     * 
     * @param top_left     The top left position of the rectangle
     * @param bottom_right The bottom right position of the rectangle
     * @param gl_mode      `GLenum` that tells how vertices should be linked together (default: `GL_LINE_LOOP`)
     */
  static void draw_rectangle(const Vector2f& top_left,
                             const Vector2f& bottom_right,
                             const GLenum gl_mode = GL_LINE_LOOP);

  /**
     * Draws a cross.
     * 
     * @param center The position of the cross
     * @param radius The 2D standard radius of the cross (might be affected by junxion weight during drawing)
     */
  static void draw_cross(const Vector2f& center, const Vector2f& radius);

  /**
     * Draws a segment.
     * Mostly here to avoid the creation of a `vector` when we only have 2 know points to draw.
     * 
     * @param p1 Coordinates of the first point
     * @param p2 Coordinates of the second segment
     */
  static void draw_segment(const Vector2f& p1, const Vector2f& p2);

  //@{
  /**
     * Draws a line.
     * 
     * @param vertices The list of points describing the line
     * @param gl_mode  `GLenum` that tells how vertices should be linked together (default: `GL_LINES`)
     */
  static void draw_lines(const vector<Vector2f>& vertices,
                         const GLenum gl_mode = GL_LINES);
  //@}

  /**
     * Draws a triangle.
     * 
     * @param vertices A flat list of points where each successive triplets describes a triangle face.
     * @param gl_mode  `GLenum` that tells how vertices should be linked together (default: `GL_TRIANGLES`)
     */
  static void draw_triangles(const vector<Vector2f>& vertices,
                             const GLenum gl_mode = GL_TRIANGLES);

  //     static void draw_quads ( const vector<Edge>& edges, const float width,
  //                              const ColorGradient color_gradient = ColorGradient(),
  //                              const GradientType gradient_type = PER_LINE );

  /**
     * Shortcut to set the `glColor4f()` out of a (255ly scaled) `sf:Color` reference.
     * 
     * @param c The input color
     */
  static void set_color(const Color& c) {
    glColor4f(c.r / 255.f, c.g / 255.f, c.b / 255.f, c.a / 255.f);
  }
};
