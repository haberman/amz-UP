#include "color_gradient.hpp"

#include <algorithm>

ColorGradient::ColorGradient(const std::vector<sf::Color>& colors) {
  if (!ramp_.empty()) {
    unsigned short i = 0;

    for (const auto& point : ramp_) {
      add_color_point(point.color, float(i) / float(ramp_.size() - 1));
      ++i;
    }
  }
}

ColorGradient::ColorGradient(const GradientStyle& gradient_type) {
  create(gradient_type);
}

void ColorGradient::add_color_point(const sf::Color& color, const float position) {
  for (unsigned int i = 0; i < ramp_.size(); i++) {
    if (position < ramp_[i].position) {
      ramp_.insert(ramp_.begin() + i, ColorPoint(color, position));
      return;
    }
  }
  ramp_.push_back(ColorPoint(color, position));
}

void ColorGradient::clear_gradient() {
  ramp_.clear();
}

void ColorGradient::create(const GradientStyle& gradient_type) {
  clear_gradient();

  switch (gradient_type) {
    case DigitalWater:
      ramp_.push_back(ColorPoint(Color(0x74ebd5ff), .0f));
      ramp_.push_back(ColorPoint(Color(0xACB6E5ff), 1.f));
      break;
    case Mango:
      ramp_.push_back(ColorPoint(Color(0xffffe259), .0f));
      ramp_.push_back(ColorPoint(Color(0xffffa751), 1.f));
      break;
    case RedSunset:
      ramp_.push_back(ColorPoint(Color(0xff355c7d), .0f));
      ramp_.push_back(ColorPoint(Color(0xff6c5b7b), .5f));
      ramp_.push_back(ColorPoint(Color(0xffc06c84), 1.f));
      break;
    case RelaxingRed:
      ramp_.push_back(ColorPoint(Color(0xfffbd5ff), .0f));
      ramp_.push_back(ColorPoint(Color(0xb20a2cff), 1.f));
      break;
    case SandBlue:
      ramp_.push_back(ColorPoint(Color(0x3e5151ff), .0f));
      ramp_.push_back(ColorPoint(Color(0xdecba4ff), 1.f));
      break;
    case Shifter:
      ramp_.push_back(ColorPoint(Color(0xbc4e9cff), .0f));
      ramp_.push_back(ColorPoint(Color(0xf80759ff), 1.f));
      break;
    case Windy:
      ramp_.push_back(ColorPoint(Color(0xacb6e5ff), .0f));
      ramp_.push_back(ColorPoint(Color(0x86fde8ff), 1.f));
      break;
    case WeddingDayBlues:
      ramp_.push_back(ColorPoint(Color(0x40e0d0ff), .0f));
      ramp_.push_back(ColorPoint(Color(0xff8c00ff), .5f));
      ramp_.push_back(ColorPoint(Color(0xff0080ff), 1.f));
      break;
  }
}

const Color ColorGradient::color_at(const float position) const {
  if (ramp_.size() == 0) {
    return Color::White;
  }

  Color c;

  unsigned int i = 0;
  for (const auto& point : ramp_) {
    const float color_position = point.position;

    if (position < color_position) {
      const ColorPoint previous_color = ramp_[max(0, (int)i - 1)];

      float position_difference = (previous_color.position - position);
      float fraction_between = (position_difference == 0) ? 0 : (position - color_position) / position_difference;

      c.r = (previous_color.color.r - point.color.r) * fraction_between + point.color.r;
      c.g = (previous_color.color.g - point.color.g) * fraction_between + point.color.g;
      c.b = (previous_color.color.b - point.color.b) * fraction_between + point.color.b;

      ++i;

      return c;
    }
  }

  c.r = ramp_.back().color.r;
  c.g = ramp_.back().color.g;
  c.b = ramp_.back().color.b;

  return c;
}

const vector<GLfloat> ColorGradient::gl_color_at(const float position) const {
  const Color color = color_at(position);
  vector<GLfloat> gl_color;

  const GLfloat red = (float)color.r / 255.f;
  gl_color.push_back(red);

  const GLfloat green = (float)color.g / 255.f;
  gl_color.push_back(green);

  const GLfloat blue = (float)color.b / 255.f;
  gl_color.push_back(blue);

  return gl_color;
}

size_t ColorGradient::size() const {
  return ramp_.size();
}
