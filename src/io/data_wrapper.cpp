/*
 * The NULL Lic3nce
 */

#include <chrono>
#include <cstring>
#include <experimental/filesystem>
#include <iostream>
#include <memory>

#include <sqlite3.h>
#include "data_wrapper.hpp"

using namespace std;
using namespace sf;

//////////////////////////////////// PUBLIC INTERFACE ////////////////////////////////////
/**
 * Creates a new instance for a given database file and assigns screen resolution value to
 * @link screen_size_ @endlink.
 *
 * @param char* db_file The database file to wrap methods around.
 */
DataWrapper::DataWrapper(const char* db_file) : db_(nullptr) {
  db_file_ = db_file;

  VideoMode mode = VideoMode::getDesktopMode();
  screen_size_.x = mode.width;
  screen_size_.y = mode.height;
}

/**
 * Destructs the instance by calling the @link close() @endlink method.
 */
DataWrapper::~DataWrapper() {
  cout << "destroying instance, closing database" << endl;
  close(true);
}

/**
 * Opens a connection to the given @link db_file_ @endlink database.
 *
 * @return bool True if connection was successfull, false otherwise.
 */
bool DataWrapper::open() {
  if (!close()) {
    return false;
  } else {
    rc_ = sqlite3_open(db_file_, &db_);
    return rc_ == SQLITE_OK;
  }
}

/**
 * Closes an open connection
 *
 * @param  destroy Indicates if we should also set the sqlite3 @link db_ @endlink instance
 *                 to nullptr.
 * @return bool    True if connection was closed without error, false otherwise.
 */
bool DataWrapper::close(bool destroy) {
  if (db_) {
    rc_ = sqlite3_close(db_);

    if (rc_ == SQLITE_OK && destroy) {
      db_ = nullptr;
    }
  } else {
    return true;
  }

  return rc_ == SQLITE_OK;
}

/**
 * Connects the database, creates the cursor table if it does not exist and prepares the
 * @link cursor_stmt_ @endlink handle.
 *
 * @return bool True if everything went fine, false otherwise.
 */
bool DataWrapper::init_cursor() {
  cout << "init cursor" << endl;

  if (!open()) return false;
  if (!check_table("cursor") && !create_cursor_table()) return false;
  if (!prepare(cursor_stmt_, "INSERT INTO cursor VALUES (?,?,?);")) return false;

  return true;
}

/**
 * Gets and saves the current mouse position.
 */
bool DataWrapper::capture_cursor() {
  busy_status_ = true;

  Vector2i mouse_position = Mouse::getPosition();

  cursor_.x = (float)mouse_position.x / (float)screen_size_.x;
  cursor_.y = (float)mouse_position.y / (float)screen_size_.y;

  auto now = chrono::time_point_cast<chrono::milliseconds>(chrono::system_clock::now());
  auto epoch = now.time_since_epoch();
  auto duration = chrono::duration_cast<chrono::milliseconds>(epoch).count();

  if (!begin()) return false;

  if (!bind(cursor_stmt_, 1, (long long int)duration)) return false;
  if (!bind(cursor_stmt_, 2, cursor_.x)) return false;
  if (!bind(cursor_stmt_, 3, cursor_.y)) return false;
  if (step(cursor_stmt_) != SQLITE_DONE) return false;

  unbind(cursor_stmt_);

  return commit();
}

/**
 * Returns every capture points within the cursor.sql file as a list of Vector2i.
 * 
 * @param points A pointer to a vector list on which found points will be pushed.
 * @param limit  An integer to limit the amount of points to query.
 *
 * @return bool True if everything went fine, false otherwise.
 */
bool DataWrapper::get_cursor_points(vector<Vector2f>* points, long long int limit) {
  busy_status_ = true;
  sqlite3_stmt* stmt = nullptr;
  const char* query = (limit == 0) ? "SELECT x,y FROM cursor;" : "SELECT x,y FROM cursor LIMIT ?;";

  if (!open()) {
    cout << "can't open" << endl;
    return false;
  }

  if (!begin()) {
    cout << "can't begin" << endl;
    return false;
  }

  if (!prepare(stmt, query)) {
    cout << "can't prepare" << endl;
    return false;
  }

  if (limit != 0 && !bind(stmt, 1, limit)) {
    return false;
  }

  if (!commit()) {
    cout << "can't commit" << endl;
    return false;
  }

  step(stmt);

  while (sqlite3_column_text(stmt, 0)) {
    Vector2f point = Vector2f();

    for (int i = 0; i < 2; i++) {
      const float value = sqlite3_column_double(stmt, i);

      if (i == 0) {
        point.x = value;
      } else if (i == 1) {
        point.y = value;
      }
    }

    step(stmt);
    points->push_back(point);
  }

  busy_status_ = false;

  return true;
}

/**
 * Returns pointer to the @link db_ @endlink sqlite3 instance.
 *
 * @return sqlite3* The pointer.
 */
sqlite3* DataWrapper::get_database() {
  return db_;
}

/**
 * Gets the @link busy_status @endlink value
 *
 * @return bool The busy status value.
 */
bool DataWrapper::get_busy_status() {
  return busy_status_;
}

/**
 * Sets the @endlink busy_status_ @endlink value
 *
 * @param busy_status The busy status value.
 */
void DataWrapper::set_busy_status(bool busy_status) {
  busy_status_ = busy_status;
}

//////////////////////////////////// PRIVATE INTERFACE ///////////////////////////////////
/**
 * Executes the given query.
 *
 * @param  query The query string to execute
 * @return bool  True if query was successfully executed, false otherwise
 */
bool DataWrapper::exec(const char* query) {
  rc_ = sqlite3_exec(db_, query, 0, 0, 0);
  return rc_ == SQLITE_OK;
}

/**
 * Begins a transaction
 *
 * @return bool True if transaction successfully began, false otherwise.
 */
bool DataWrapper::begin() {
  return exec("BEGIN");
}

/**
 * Commits a transaction.
 *
 * @return bool True if transaction was successfully commited, false otherwise.
 */
bool DataWrapper::commit() {
  return exec("COMMIT");
}

/**
 * Prepares a statement object
 *
 * @param  statement The sqlite3_stmt handle to associate the query compilation with.
 * @param  query     The query string to prepare.
 * @return bool      True if the query's compilation was successfull, false otherwise.
 */
bool DataWrapper::prepare(sqlite3_stmt*& statement, const char* query) {
  rc_ = sqlite3_prepare_v2(db_, query, strlen(query), &statement, 0);
  return rc_ == SQLITE_OK;
}

/**
 * Evaluates the given prepared statement
 *
 * @param  statement The sqlite3_stmt handle to evaluate.
 * @return unsigned       Integer value returned by sqlite3_step.
 */
int DataWrapper::step(sqlite3_stmt* statement) {
  rc_ = sqlite3_step(statement);
  return rc_;
}

/**
 * Binds an integer to a prepared statement
 *
 * @param  statement The sqlite3_stmt handle.
 * @param  index     The wildcard's index where to bind the integer to.
 * @param  value     The integer value to bind.
 * @return bool      True if binding was succesfull, false otherwise.
 */
bool DataWrapper::bind(sqlite3_stmt* statement, int index, int value) {
  rc_ = sqlite3_bind_int(statement, index, value);
  return rc_ == SQLITE_OK;
}

/**
 * Binds a 64 bytes integer to a prepared statement
 *
 * @param  statement The sqlite3_stmt handle.
 * @param  index     The wildcard's index where to bind the int64 to.
 * @param  value     The int64 value to bind.
 * @return bool      True if binding was succesfull, false otherwise.
 */
bool DataWrapper::bind(sqlite3_stmt* statement, int index, long long int value) {
  rc_ = sqlite3_bind_int64(statement, index, value);
  return rc_ == SQLITE_OK;
}

/**
 * Binds a double to a prepared statement
 *
 * @param  statement The sqlite3_stmt handle.
 * @param  index     The wildcard's index where to bind the double to.
 * @param  value     The double value to bind.
 * @return bool      True if binding was succesfull, false otherwise.
 */
bool DataWrapper::bind(sqlite3_stmt* statement, int index, double value) {
  rc_ = sqlite3_bind_double(statement, index, value);
  return rc_ == SQLITE_OK;
}

/**
 * Binds a string to a prepared statement
 *
 * @param  statement The sqlite3_stmt handle.
 * @param  index     The wildcard's index where to bind the string to.
 * @param  value     The string value to bind.
 * @return bool      True if binding was succesfull, false otherwise.
 */
bool DataWrapper::bind(sqlite3_stmt* statement, int index, const char* value) {
  rc_ = sqlite3_bind_text(statement, index, value, strlen(value), SQLITE_STATIC);
  return rc_ == SQLITE_OK;
}

/**
 * Resets, clears bindings and optionaly finalizes the given statement.
 *
 * @param  statement The sqlite3_stmt handle.
 * @param  finalize  Whether we should destroy statement or not.
 * @return bool      True if each step went fine, false otherwise.
 */
bool DataWrapper::unbind(sqlite3_stmt* statement, bool finalize) {
  rc_ = sqlite3_reset(statement);
  if (rc_ != SQLITE_OK) {
    return false;
  }

  rc_ = sqlite3_clear_bindings(statement);
  if (rc_ != SQLITE_OK) {
    return false;
  }

  if (finalize) {
    rc_ = sqlite3_finalize(statement);
  }

  return rc_ == SQLITE_OK;
}

/**
 * Checks if a given table is present on the database
 *
 * @param  table_name The name of the table to check.
 * @return bool       True if table exists, false otherwise.
 */
bool DataWrapper::check_table(const char* table_name) {
  const char* query = "SELECT name FROM sqlite_master WHERE type='table' AND name=?;";
  sqlite3_stmt* stmt = nullptr;

  if (!prepare(stmt, query)) {
    return false;
  }

  if (!bind(stmt, 1, table_name)) {
    return false;
  }

  step(stmt);

  return rc_ == SQLITE_ROW;
}

/**
 * Creates the cursor table
 *
 * @return bool True if the table was successfully created, false otherwise.
 */
bool DataWrapper::create_cursor_table() {
  const char* query = "CREATE TABLE cursor (time INTEGER PRIMARY KEY NOT NULL, x REAL NOT NULL, y REAL NOT NULL)";

  if (!begin()) {
    return false;
  }

  if (!exec(query)) {
    return false;
  }

  return commit();
}
