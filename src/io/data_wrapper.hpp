/*
 * The NULL Lic3nce
 */

#ifndef DATAWRAPPER_H
#define DATAWRAPPER_H

#include <sqlite3.h>
#include <SFML/Window.hpp>

using namespace std;

class DataWrapper {
 private:
  bool busy_status_;

  const char* db_file_;
  int rc_;

  sqlite3* db_;
  sqlite3_stmt* cursor_stmt_;

  sf::Vector2u screen_size_;
  sf::Vector2f cursor_;

  bool exec(const char* query);
  bool begin();
  bool commit();
  bool prepare(sqlite3_stmt*& statement, const char* query);
  int step(sqlite3_stmt* statement);

  bool bind(sqlite3_stmt* statement, int index, int value);
  bool bind(sqlite3_stmt* statement, int index, long long int value);
  bool bind(sqlite3_stmt* statement, int index, double value);
  bool bind(sqlite3_stmt* statement, int index, const char* value);
  bool unbind(sqlite3_stmt* statement, bool finalize = false);

  bool check_table(const char* table_name);

  bool create_cursor_table();
  bool create_island_table();

 public:
  DataWrapper(const char* db_file);
  ~DataWrapper();

  bool open();
  bool close(bool destroy = false);

  bool init_cursor();
  bool capture_cursor();
  bool get_cursor_points(vector<sf::Vector2f>* points, long long int limit = 0);

  bool get_busy_status();
  void set_busy_status(bool busy_status);

  sqlite3* get_database();
};

#endif  // DATAWRAPPER_H
