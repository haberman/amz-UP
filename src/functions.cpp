#include "functions.hpp"

#include <iostream> // cout
#include <chrono>   // chrono::system_clock
#include <sstream>  // stringstream
#include <iomanip>  // put_time

string Functions::basename ( const string& pathname, const bool remove_ext )
{
    // Remove directory if present.
    string filename = pathname;

    const size_t last_slash_idx = pathname.find_last_of ( "\\/" );

    if ( string::npos != last_slash_idx ) {
        filename.erase ( 0, last_slash_idx + 1 );
    }


    if ( remove_ext ) {
        // Remove extension if present.
        const size_t period_idx = filename.rfind ( '.' );
        if ( string::npos != period_idx ) {
            filename.erase ( period_idx );
        }
    }

    return filename;
}

void Functions::save_screenshot ( sf::Window& window, const string output_folder, const string name, const bool verbose )
{
    auto now = chrono::system_clock::now();
    auto in_time_t = chrono::system_clock::to_time_t ( now );

    stringstream ss;
    ss << output_folder << name << put_time ( localtime ( &in_time_t ), "_%Y-%m-%d_%X.jpg" );

    Vector2u windowSize = window.getSize();
    Texture texture;

    texture.create ( windowSize.x, windowSize.y );
    texture.update ( window );

    Image screenshot = texture.copyToImage();
    screenshot.saveToFile ( ss.str() );

    if ( verbose ) {
        cout << "Screenshot saved: " << ss.str() << endl;
    }
}