#pragma once

#include "../graphics/color_gradient.hpp"

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Projection_traits_xy_3.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Delaunay_triangulation_adaptation_traits_2.h>
#include <CGAL/Delaunay_triangulation_adaptation_policies_2.h>

#include <SFML/Graphics.hpp>
#include <iostream>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Projection_traits_xy_3<K> PJ;

typedef CGAL::Delaunay_triangulation_2<PJ> DT;
typedef CGAL::Delaunay_triangulation_adaptation_traits_2<DT> AT;
typedef CGAL::Delaunay_triangulation_caching_degeneracy_removal_policy_2<DT> AP;

using namespace sf;

/** @addtogroup cgal
 *  @{*/

/** 
 * This class pipes input points to different `CGAL` [triangulation](https://doc.cgal.org/latest/Triangulation_2/index.html#Chapter_2D_Triangulations)'s components.
 * TODO -> Make a composition out of 2 triangulation's methods: a (more expressive) Delaunay and a constrained one.
 */
class Triangulator
{
public:
  /**
   * Constructor.
   * 
   * @param fill_gradient A `ColorGradient` instance used to map a ramp of colors to a 3rd value input
   */
  Triangulator(shared_ptr<ColorGradient> fill_gradient) : fill_gradient_(fill_gradient){};

  /** Destructor. */
  ~Triangulator();

  /**
   * Insert a new point.
   * 
   * @param point A `sf::Vectorf2f` coordinate
   */
  void insert(const Vector2f &point);

  /**
   * Draws the Delaunay triangulation.
   * 
   * @param fill Whether to draw the triangulation as outlines or filled areas
   */
  void draw(const bool fill = false);

private:
  /** A `vector` of `sf::Vector2f` holding points on which calculations are made. */
  vector<Vector2f> points_;

  /** Shared pointer to a ColorGradient instance used to disseminate colors within the drawing. */
  shared_ptr<ColorGradient> fill_gradient_;

  /**
   * The `CGAL` delaunay triangulation instance.
   * @see https://doc.cgal.org/latest/Triangulation_2/index.html#Section_2D_Triangulations_Delaunay
   */
  DT delaunay_;

  /** Keeps track of the smallest face dimension. */
  float max_area_;

  /** Keeps track of the biggest face dimension. */
  float min_area_;

  /** Computes triangulation on _points. */
  void compute_();
};
/**@}*/
