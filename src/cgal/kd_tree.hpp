#pragma once

#include "../functions.hpp"
#include "../graphics/color_gradient.hpp"
#include "../graphics/gl_drawer.hpp"
#include "../io/data_wrapper.hpp"
// #include "../utils/projector.hpp"
#include "kd_node.hpp"

#include <SFML/OpenGL.hpp>

#include <future>
#include <memory>
#include <queue>

/** @addtogroup cgal
 *  @{*/

/** 
 * This class wraps a `CGAL` [Kd_tree](https://doc.cgal.org/latest/Spatial_searching/classCGAL_1_1Kd__tree.html) and exposes methods that facilitates traversing.
 * Warning: the drawing uses raw input points' coordinates; therefore, the visible region is ranged between -1 & 1.
 * 
 * TODO -> well, draw the shit!
 */
template <typename T>
class KdTree {
 public:
  /**
   * Constructor for T<int>
   * 
   * @param theme_gradient A ColorGradient instance used to map a ramp of colors to a 3rd value input
   * @param texture_space Space on which to draw
   * @param dimensions Amount of possible cuts
   */
  KdTree(shared_ptr<ColorGradient> theme_gradient,
         shared_ptr<IntRect> texture_space,
         const unsigned dimensions = 2) : theme_gradient_(theme_gradient),
                                          dimensions_(dimensions) {
    root_space_ = new Rect<T>((T)texture_space->left, (T)texture_space->top,
                              (T)texture_space->width, (T)texture_space->height);
  }

  /**
   * Constructor for T<float>
   * 
   * @param theme_gradient A ColorGradient instance used to map a ramp of colors to a 3rd value input
   * @param dimensions Amount of possible cuts
   */
  KdTree(shared_ptr<ColorGradient> theme_gradient,
         const unsigned dimensions = 2) : theme_gradient_(theme_gradient),
                                          dimensions_(dimensions) {
    root_space_ = new Rect<T>(-1.f, 1.f, 2.f, -2.f);
  }

  /** Destructor. */
  ~KdTree() {
    delete root_;
    delete root_space_;

    theme_gradient_.reset();
  }

  /**
     * Inserts a new point.
     * 
     * @param reference to `SFML::Vector2f`
     */
  void insert(const Vector2<T>& point) { points_.push_back({point.y, point.x}); }

  /**
     * Draws the KD diagram.
     * 
     * @param render_texture Pointer to an `SFML` texture on which the tree should be drawn.
     * @param fill Whether to draw as outlines or filled areas
     */
  void draw(RenderTexture* render_texture, const bool fill = false) {
    compute_();

    const float deepness = (float)KdNode<T>::deepness(root_);

    cout << "Deepness: " << deepness << endl;

    queue<KdNode<T>*> queue;
    queue.push(root_);

    unsigned short i = 0;
    while (!queue.empty()) {
      if (i > 1000) break;

      KdNode<T>* current_node = queue.front();
      Rect<T>* bbox = current_node->bbox();
      const T* tuple = current_node->tuple();

      stringstream ss;

      Vector2<T> p1, p2;       //, m;
      if (current_node->axis)  // 1 -> y
      {
        p1.y = p2.y = tuple[1];
        ss << "[Y]: " << tuple[1];

        p1.x = bbox->left;
        p2.x = bbox->left + bbox->width;

        // m.x = (p1.x + p2.x) * .5f;
      } else {
        p1.x = p2.x = tuple[0];
        ss << "[X]: " << tuple[0];

        p1.y = bbox->top;
        p2.y = bbox->top + bbox->height;

        // m.y = (p1.y + p2.y) * .5f;
      }

      // if (current_node->depth == 3) {
      // GLDrawer::set_color(Color::Red);
      // cout << "cut 3: " << ss.str() << " type: " << current_node->type << endl;
      // m.x = Functions::map_number(m.x, .0f, 1.f, -1.f, 1.f);
      // m.y = Functions::map_number(m.y, .0f, 1.f, 1.f, -1.ff);
      // GLDrawer::draw_text(render_texture, m, ss.str(), *DataBuffer::get_instance()->monoid_font);
      // }
      // else
      // {
      GLDrawer::set_color(theme_gradient_->color_at((float)current_node->depth / deepness));
      // }

      // GLDrawer::set_color(sf::Color::White);
      GLDrawer::draw_segment(p1, p2);

      if (current_node->lower()) queue.push(current_node->lower());
      if (current_node->upper()) queue.push(current_node->upper());

      queue.pop();
      ++i;
    }
  }

 private:
  /**
     * Pointer to a `CGAL` Kd_tree diagram instance.
     * 
     * @see https://doc.cgal.org/latest/Spatial_searching/classCGAL_1_1Kd__tree.html
     */
  KdNode<T>* root_ = nullptr;
  Rect<T>* root_space_ = nullptr;

  /** A `vector` of `sf::Vector2f` holding points on which calculations are made. This permits keeping the `CGAL` graph logic separated from data. */
  vector<vector<T>> points_;

  /** Shared pointer to a `ColorGradient` instance used to disseminate colors within the drawing. */
  shared_ptr<ColorGradient> theme_gradient_;

  /** The amount of "cuttable" dimensions. */
  unsigned int dimensions_;

  /** The depth of the entire tree. */
  unsigned int depth_ = 0;

  unsigned int count_ = 0;

  /**
   * Builds the tree. Internally called before every `draw()`.
   * @TODO -> it's bad, protect it with some kind of a `computed_` bool.
  */
  void compute_() {
    cout << "COMPUTE" << endl
         << "  -> top-left: " << root_space_->left << " " << root_space_->top << endl
         << "  -> bottom-right: " << root_space_->left + root_space_->width << " " << root_space_->top + root_space_->height << endl;

    // const unsigned short num_points = 16;
    const unsigned short num_points = points_.size();

    vector<T*> tuples_vector(num_points);
    for (unsigned int j = 0; j < num_points; ++j) {
      tuples_vector.at(j) = &(points_.at(j).at(0));
    }

    root_ = KdNode<T>::create_tree(tuples_vector, dimensions_, root_space_);
  }
};
/**@}*/
