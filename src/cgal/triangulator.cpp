#include "../functions.hpp"
#include "../graphics/gl_drawer.hpp"

#include "triangulator.hpp"

#include <SFML/OpenGL.hpp>

Triangulator::~Triangulator() {
  if (delaunay_.is_valid()) {
    delaunay_.clear();
  }
}

void Triangulator::insert(const sf::Vector2f& point) {
  points_.push_back(point);
}

void Triangulator::compute_() {
  vector<K::Point_3> points;

  for (auto it = points_.begin(); it != points_.end(); ++it) {
    points.push_back(K::Point_3(it->x, it->y, .0f));
  }

  delaunay_ = DT(points.begin(), points.end());

  for (auto it = delaunay_.finite_faces_begin(); it != delaunay_.finite_faces_end(); ++it) {
    const DT::Triangle triangle = delaunay_.triangle(it);

    min_area_ = min(min_area_, (float)triangle.squared_area());
    max_area_ = max(max_area_, (float)triangle.squared_area());
  }
}

void Triangulator::draw(const bool fill) {
  compute_();

  for (auto it = delaunay_.finite_faces_begin(); it != delaunay_.finite_faces_end(); ++it) {
    const DT::Triangle triangle = delaunay_.triangle(it);

    vector<sf::Vector2f> vertices;
    for (unsigned short i = 0; i < 3; ++i) {
      vertices.push_back(Vector2f(triangle[i].x(), triangle[i].y()));
    }

    const float v = Functions::map_number((float)triangle.squared_area(),
                                          min_area_, max_area_, .0f, 1.f);

    const vector<GLfloat> fill_color = fill_gradient_->gl_color_at(v);

    if (fill) {
      glColor3f(fill_color[0], fill_color[1], fill_color[2]);
      GLDrawer::draw_triangles(vertices);
    }

    GLDrawer::set_color(sf::Color::White);
    GLDrawer::draw_lines(vertices, GL_LINE_STRIP);
  }
}