#include "../functions.hpp"
#include "../graphics/gl_drawer.hpp"

#include "voronoi.hpp"

#include <SFML/OpenGL.hpp>

Voronoi::~Voronoi() {
  if (voronoi_.is_valid()) {
    voronoi_.clear();
  }
}

void Voronoi::insert(const sf::Vector2f& point) {
  points_.push_back(point);
}

void Voronoi::compute_() {
  vector<AT::Site_2> sites;
  for (const auto& point : points_) {
    sites.push_back(AT::Site_2(point.x, point.y, .0f));
  }

  voronoi_ = VD(sites.begin(), sites.end());
}

void Voronoi::draw(const bool fill) {
  compute_();

  for (auto it = voronoi_.bounded_faces_begin(); it != voronoi_.bounded_faces_end(); ++it) {
    const VD::Ccb_halfedge_circulator edge_start = it->ccb();
    VD::Ccb_halfedge_circulator edge_circulator = edge_start;

    vector<sf::Vector2f> points;

    do {
      const float x = ((VD::Halfedge_handle)edge_circulator)->source()->point().x();
      const float y = ((VD::Halfedge_handle)edge_circulator)->source()->point().y();
      points.push_back(sf::Vector2f(x, y));
    } while (++edge_circulator != edge_start);

    GLDrawer::draw_lines(points, GL_LINE_STRIP);
  }
}
