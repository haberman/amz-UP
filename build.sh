#!/bin/bash

if ! type getopts > /dev/null; then
    echo "getopts failed in this environment."
    exit 1
fi

while getopts cde arg; do
    case $arg in
        c)
            clean=1
            ;;
        d)
            debug=1
            ;;
        e)
            exec=1
            ;;
        :)
            echo "Option: $OPTARG requires an argument"
            exit 1
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            exit 1
            ;;
    esac
done

shift $(($OPTIND -1))

if [[ -z $1 ]]
then
    echo "Missing CMAKE directory."
    exit 1
fi

if [[ ! -z $debug ]]
then
    build=Debug
else
    build=Release
fi

printf "[BUILD]> $1 ($build) ***\n"

if [[ ! -z $clean ]]
then
    printf "\n[build]> clean...\n"
    rm -rf build
    mkdir build
fi

cd build

printf "\n[build]> make...\n"

export CC=/usr/bin/gcc
export CXX=/usr/bin/g++
cmake -DCMAKE_BUILD_TYPE=$build -DCMAKE_PROJECT_NAME=$1 -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -G Ninja ..

printf "\n[build]> compile...\n"
ninja

if [[ ! -z $exec ]]
then
    printf "\n[build]> execute $1-$build...\n"
    ./$1-$build
fi
